package daspro.recruitment;
import java.util.Scanner ;

public class No5 {
        public static void main(String[] args) {

        Scanner in = new Scanner(System.in);
        int output = 0;
        int N = in.nextInt();
        if (N >= 1 && N <= 100000) {
            int[] Ai = new int[N + 1];
            for (int i = 1; i <= N; i++) {
                Ai[i] = i;
                System.out.print(Ai[i] + " ");
                output+=Ai[i];
            }
            System.out.println("");
            int Q = in.nextInt();
            if (Q >= 0 && Q <= 1000000) {
                int[] X = new int[Q];
                for (int i = 0; i < X.length; i++) {
                    X[i] = in.nextInt();
                    if(X[i]==0){
                       X[i]=1; 
                    }
                    if (X[i] > N || X[i] < 0) {
                        break;
                    }
                    output *= 2*X[i];
                }
                System.out.println((int)output%(int)(Math.pow(10, 9)+7));
            }
        }
    }
}